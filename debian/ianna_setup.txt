ianna is running on winnie.postgis.net virtual box ssh port 50022
(I think I created from https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-9.2.1-amd64-netinst.iso ) dated Oct 13th-2017
experimental gitea is installed on her and accessible via:
https://winnie.postgis.net/gitea

sudo bash
apt-get install default-jdk
apt-get install autoconf libtool build-essential
apt-get install libreadline6 libreadline6-dev zlib1g-dev
apt-get install libcunit1-dev libcunit1-doc libcunit1
apt-get install docbook
apt-get install libxml2-dev n xsltproc docbook-xsl docbook-mathml gettext 
apt-get install libjson-c-dev
apt-get install dblatex
apt-get install imagemagick
apt-get install libxml2 libxml2-dev
apt-get install bison 
apt-get install libiconv-hook1 
apt-get install subversion
apt-get install proj
apt-get install postgresql-server-dev-9.6
apt-get install libgeos-3.5 libgeos-dev libproj-dev
apt-get install libgdal-dev
apt-get install pkg-config
#for code coverage testing
sudo apt-get install lcov

#for docker
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
#turn on unattended upgrades
apt-get install unattended-upgrades apt-listchanges
#manual test
unattended-upgrade -d

#installing buildbot
