## begin variables passed in by jenkins

# export OS_BUILD=64
# export WORKSPACE=/var/lib/jenkins/workspace/geos 
  # (it is already cd.. to WORKSPACE)
# export GEOS_VER=3.4.2dev
# export GEOS_MINOR_VER=3.4
# export GDAL_VER=2.0

## end variables passed in by jenkins
cd ${WORKSPACE}/branches/${GEOS_MINOR_VER}
sh autogen.sh
if [ -e ./GNUMakefile ]; then
 make distclean
fi

./configure --prefix=${WORKSPACE}/rel-${GEOS_VER}w${OS_BUILD}
make
rm -rf ${WORKSPACE}/rel-${GEOS_VER}w${OS_BUILD}
make install
make check