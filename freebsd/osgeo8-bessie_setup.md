Bessie currently setup on winnie's virtual box - ssh port ssh your_user_name@winnie.postgis.net -p 51022
Built from wget https://download.freebsd.org/ftp/releases/VM-IMAGES/14.1-RELEASE/amd64/Latest/FreeBSD-14.1-RELEASE-amd64-zfs.qcow2.xz dated (Oct 2024)

vi /etc/rc.conf

add to file

hostname="bessie"
sshd_enable="YES"

#then edit the sshd_config
vi /etc/ssh/sshd_config 

Add line
AllowGroups wheel
and add to AllowUsers list

vi /etc/ssh/sshd_config #:g/AllowUsers/, :g/PermitRootLogin #change to yes so can configure - can change later to no

#Make sure jenkins is in AllowUsers list of /sshd_config
cat /etc/ssh/sshd_config | grep AllowUsers #if jenkins not listed add



Then run:
service sshd restart

#if get host keys not found run
ssh-keygen -A

To create and add a user robe (repeat for jenkins, pramsey, strk, komzpa, cvvergara, Algunenano) and install relevant ssh keys in homedirs

adduser robe
su robe
mkdir ~/.ssh
echo ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAmf79bn/i3jeP9IuFZ0BXGTJundqVLW5gRUfnG2cPvmDtGRnvKZ/cEoxDbuvh9vyQhV/wn3Y1ACgBc+lwhJUXmjZDq2ft4iL0GfOpjyEwT+QohSyShy9lijCJ2iNdxG1ixZokXReYkyldSgjSpMzTbtaAZCx0rMk9zY/3lQM+IR3OTTv8HUmzECFwgA6fcjKnzq98Ng2fdf/1tcvz73YfVZlo92Q0pSsGFhU4ytsI7CqVF9i5JdFlUToOB+KXQpsizQ3XeDgp+kxcOGHiXHbZb3h5w7yy7J7+cwrWOrZxCEMfHITy7NQcRj3bbK2B+pPVG87/oHTzAGgDbcZYq6uzLQ== robe2048 >> ~/.ssh/authorized_keys


adduser jenkins
su jenkins
mkdir ~/.ssh
echo ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAmf79bn/i3jeP9IuFZ0BXGTJundqVLW5gRUfnG2cPvmDtGRnvKZ/cEoxDbuvh9vyQhV/wn3Y1ACgBc+lwhJUXmjZDq2ft4iL0GfOpjyEwT+QohSyShy9lijCJ2iNdxG1ixZokXReYkyldSgjSpMzTbtaAZCx0rMk9zY/3lQM+IR3OTTv8HUmzECFwgA6fcjKnzq98Ng2fdf/1tcvz73YfVZlo92Q0pSsGFhU4ytsI7CqVF9i5JdFlUToOB+KXQpsizQ3XeDgp+kxcOGHiXHbZb3h5w7yy7J7+cwrWOrZxCEMfHITy7NQcRj3bbK2B+pPVG87/oHTzAGgDbcZYq6uzLQ== robe2048 >> ~/.ssh/authorized_keys

echo ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMWfil4H34fePXGfx0uR6Gd9TnHTgKKYPJLmaMsTicmx vicky@pgvicky  >> ~/.ssh/authorized_keys


#when prompted make sure to add to wheel group

#for adds after user created
pw group mod wheel -m robe

#Now robe can do 

su root 

(to do admin stuff)
#upgrade to latest patches
freebsd-update fetch

#installing ports
portsnap fetch
portsnap extract

#in future to update ports
portsnap fetch
portsnap update

pkg install sudo

Then run:

visudo

and uncomment line wheel nopasswd allowing wheel to use sudo without password.

pkg install jed bash tcpdump wget git proj geos
#had these installed, didn't work but worked with openjdk8
pkg install openjdk17

mount -t fdescfs fdesc /dev/fd
mount -t procfs proc /proc


#add following lines to /etc/fstab
fdesc   /dev/fd         fdescfs         rw      0       0
proc    /proc           procfs          rw      0       0

pkg install gmake gdb autoconf libtool curl automake expect
pkg install libxslt libxml2 bison
pkg install imagemagick7
pkg install gdal geos proj pkgconf
projsync --system-directory --source-id us_noaa
projsync --system-directory --source-id ch_swisstopo
pkg install sfcgal json-c
pkg install cunit iconv
pkg install protobuf protobuf-c  #needed for building mvt
ldconfig

#was going to install postgresql in jail but got lost quickly
#did these steps to enable jails in future
sysctl security.jail.sysvipc_allowed=1

#Make it persistent by adding this line to /etc/sysctl.conf

security.jail.sysvipc_allowed=1

#Add this line to /etc/rc.conf

jail_sysvipc_allow="YES"

#Enter your jail and install Postgres.


pkg install postgresql16-server postgresql16-contrib gdal
service postgresql initdb #create data cluster

edit the /etc/rc.conf 

and add a line
postgresql_enable="YES" 

pkg update #update available list
pkg upgrade #upgrades all packages

pkg info proj  #get details of proj
pkg install cmake #install needed for building pgrouting and sfcgal in future

#for code coverage testing
pkg install lcov

#for pgtap tests needed for pgrouting
git clone https://github.com/theory/pgtap.git
cd pgtap
gmake
sudo gmake install
sudo cpan TAP::Parser::SourceHandler::pgTAP
sudo gmake installcheck PGUSER=postgres



# purge upgrade files
cd /var/db/freebsd-update/files
rm -rf [0-1]*
rm -rf [1-3]*
rm -rf [3-5]*
rm -rf [5-7]*
rm -rf [0-9]*
rm -rf [a-b]*
rm -rf [a-d]*
rm -rf [a-h]*
```

# install agent jar
```
su jenkins
mkdir bin
cd bin
#can't use ssh for launching agent because we need bastion
# and when using the launch command, doesnt' automatically deploy agent
curl -sO https://debbie.postgis.net/jnlpJars/agent.jar

#then on jenkins config use  Launch agent via execution of comman on the controller

launch command:ssh jenkins@osgeo8-bessie " java -jar /home/jenkins/bin/agent.jar"
```