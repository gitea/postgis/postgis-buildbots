#!/bin/bash
set -e
#--passed in by Jenkins
#export OS_BUILD=64
#export GCC_TYPE=
#export PG_VER=15
#export LIBXML_VER=2.7.8
#export PGPORT=5443
#export H3PG_VER=
if [ "$GCC_TYPE" == "121" ]; then
	export GCC_TYPE=
fi
export PROJECTS=/projects

export PGPATH=${PROJECTS}/postgresql/rel/pg${PG_VER}w${OS_BUILD}${GCC_TYPE}
export PGPATHEDB=${PGPATH}edb

export PGBIN=${PGPATH}/bin
echo "PGBIN is $PGBIN"
export PATH="/mingw/bin:/mingw/include:/mingw/lib:/c/Windows/system32:/bin:/include:/usr/local/bin"
export PATH="${PATH}:${PROJECTS}/zlib/rel-zlib-${ZLIB_VER}w${OS_BUILD}${GCC_TYPE}/bin:${PROJECTS}/zlib/rel-zlib-${ZLIB_VER}w${OS_BUILD}${GCC_TYPE}/lib:${PROJECTS}/zlib/rel-${ZLIB_VER}w${OS_BUILD}${GCC_TYPE}/include:${PROJECTS}/libxml/rel-libxml2-${LIBXML_VER}w${OS_BUILD}${GCC_TYPE}/bin:${PROJECTS}/libxml/rel-libxml2-${LIBXML_VER}w${OS_BUILD}${GCC_TYPE}/lib:${PROJECTS}/libxml/rel-libxml2-${LIBXML_VER}w${OS_BUILD}${GCC_TYPE}/include:${PGPATH}/include:${PGPATH}/bin:/cmake/bin"
#export PGPORT=5452
export PGUSER=postgres
export SOURCES=/sources
cd /projects
cd h3-pg/h3-pg/
make
make install
ctest --test-dir build --output-on-failure --build-config Release

#git clean -fdx
export GIT_REVISION=`git status | head -1`
export RELDIR=/projects/h3-pg/builds
export RELVERDIR=h3-pg_pg${PG_VER}-binaries-${H3PG_VER}w${OS_BUILD}
export outdir="${RELDIR}/${RELVERDIR}"
export package="${RELDIR}/${RELVERDIR}.zip"
export INSTALL_DIR=${RELDIR}/${RELVERDIR}
export verfile="${INSTALL_DIR}/h3-pg_version.txt"


rm -rf ${INSTALL_DIR}
mkdir -p ${INSTALL_DIR}/lib
mkdir -p ${INSTALL_DIR}/docs
mkdir -p ${INSTALL_DIR}/share/extension 
cp -r ${RELDIR}/packaging_notes/* ${INSTALL_DIR}/
cp build/h3/h3*.dll ${INSTALL_DIR}/lib
cp build/h3_postgis/h3*.dll ${INSTALL_DIR}/lib
cp ${PGPATH}/share/extension/h3*.control ${INSTALL_DIR}/share/extension
cp ${PGPATH}/share/extension/h3*.sql ${INSTALL_DIR}/share/extension

cp README.md ${INSTALL_DIR}
cp LICENSE ${INSTALL_DIR}
cp docs/*.md ${INSTALL_DIR}/docs/
	
echo "H3-PG extensions: ${H3PG_VER}" > $verfile
echo "PostgreSQL: ${PG_VER} w${OS_BUILD}" >> $verfile
date_built="`eval date +%Y%m%d`"
echo "Built: ${date_built}" >> $verfile
echo "GIT_REPO: https://github.com/zachasme/h3-pg" >> $verfile 
echo "GIT_REVISION: ${GIT_REVISION}" >> $verfile

# steps for zipping
cd ${RELDIR}
zip -r $package ${RELVERDIR}
cp $package ${PROJECTS}/postgis/win_web/download/windows/pg${PG_VER}/buildbot/extras