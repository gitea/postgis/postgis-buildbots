# passed in by jenkins 
# export GEOS_VER=3.4.1dev 
# export PROJECTS=/c/jenkins 
export SH_WORKSPACE=${PROJECTS}/geos

if [[ "${GCC_TYPE}" == "gcc48" ]] ; then
	export PROJECTS=/projects
	export PATHOLD=$PATH
else
	export PROJECTS=/projects
	export PATHOLD=$PATH
fi;

if [ "$OS_BUILD" == "64" ] ; then
	export MINGHOST=x86_64-w64-mingw32
else
	export MINGHOST=i686-w64-mingw32
fi;
CMAKE_PATH=/cmake
export PATHOLD="/mingw/bin:/mingw/include:/mingw/lib:/c/Windows/system32:/c/Windows:.:/bin:/include:/usr/local/bin:/c/ming${OS_BUILD}/svn"


#CPPFLAGS="-I${PGPATH}/include -I${MINGPROJECTS}/rel-libiconv-1.13.1w${OS_BUILD}/include" \
#LDFLAGS="-L${PGPATH}/lib -L${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}/lib -L${MINGPROJECTS}/rel-libiconv-1.13.1w${OS_BUILD}/lib" 
  if [ "$GCC_TYPE" == "gcc48" ]; then
    export PATH="${PATH}:${CMAKE_PATH}/bin:/bin:/include"
    if [[ "${GEOS_VER}"  == *SVN* || "${GEOS_VER}"  == *dev* ]] ; then
      export SRC_DIR=${PROJECTS}/geos/branches/${GEOS_MINOR_VER}cmake
    else
      #tagged version -- official release
      export SRC_DIR=${PROJECTS}/geos/tags/${GEOS_MINOR_VER}cmake
    fi;
  
    echo $SRC_DIR
    export PATH="${PATH}:${CMAKE_PATH}/bin:/bin:/include"
    #if building from svn
    cd $SRC_DIR
    tools/svn_repo_revision.sh 
    cd ../
    rm -rf build${OS_BUILD}${GEOS_VER}
    mkdir -p build${OS_BUILD}${GEOS_VER}
    cd build${OS_BUILD}${GEOS_VER}
  
  if [[ "${OS_BUILD}" == "64" ]] ; then
    cmake -G "MSYS Makefiles" DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=${PROJECTS}/geos/rel-${GEOS_VER}w${OS_BUILD}${GCC_TYPE} -DHAVE_STD_ISNAN=1 -DHAVE_LONG_LONG_INT_64=1 CMAKE_BUILD_TYPE=Release -DGEOS_ENABLE_INLINE=NO -DDISABLE_GEOS_INLINE=ON  -DGEOS_ENABLE_TESTS=ON -  ../${GEOS_MINOR_VER}cmake
  else
     cmake -G "MSYS Makefiles" DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=${PROJECTS}/geos/rel-${GEOS_VER}w${OS_BUILD}${GCC_TYPE} -DHAVE_STD_ISNAN=1 -DGEOS_ENABLE_INLINE=NO CMAKE_BUILD_TYPE=Release -DGEOS_ENABLE_TESTS=ON -DDISABLE_GEOS_INLINE=ON -  ../${GEOS_MINOR_VER}cmake
  fi
  make && make install
  make check
else
    if [[ "${GEOS_VER}"  == *SVN* || "${GEOS_VER}"  == *dev* ]] ; then
      export SRC_DIR=${PROJECTS}/geos/branches/${GEOS_MINOR_VER}
    else
      #tagged version -- official release
      export SRC_DIR=${PROJECTS}/geos/tags/${GEOS_MINOR_VER}
    fi;
  if [ -e ./GNUMakefile ]; then
    make distclean
  fi
  sh autogen.sh 
  make distclean 
  ./configure --target=${MINGHOST} --prefix=${SH_WORKSPACE}/rel-${GEOS_VER}w${OS_BUILD}${GCC_TYPE}
  make clean 
  make 
  make install 
  strip ${SH_WORKSPACE}/rel-${GEOS_VER}w${OS_BUILD}${GCC_TYPE}/bin/*.dll 
  cd ${SRC_DIR}
  make check 

fi

