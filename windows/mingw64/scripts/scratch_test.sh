#!/bin/sh

rm -rf /tmp/src
mkdir /tmp/src
touch /tmp/src/file

rm -rf /tmp/tgt
ln -Tsf /tmp/src /tmp/tgt
file /tmp/tgt
ln -Tsf /tmp/src /tmp/tgt
echo "Test with ln -Tsf /tmp/src /tmp/tgt done twice"
ls -l /tmp/tgt

rm -rf tmp/tgt
ln -sf /tmp/src /tmp/tgt
file /tmp/tgt
ln -sf /tmp/src /tmp/tgt
echo "Test with ln -sf /tmp/src /tmp/tgt done twice"
ls -l /tmp/tgt

rm -rf /tmp/tgt
mkdir /tmp/tgt
ln -sf /tmp/src /tmp/tgt/
file /tmp/tgt/src
ln -sf /tmp/src /tmp/tgt/
echo "Test with ln -sf /tmp/src /tmp/tgt/ done twice"
ls -l /tmp/tgt/src/

echo "Done"
