#!/bin/sh

#
# USAGE:
#
# -- postgis-cvs.tar.gz 
# sh make_dist.sh
#
# -- postgis-1.1.0.tar.gz 
# sh make_dist.sh 1.1.0
#
# NOTE: will not work prior to 1.1.0
#
#
# Remarked out variables are set by jenkins
# OS_BUILD=64
PROJECTS=/c/jenkins
MINGPROJECTS=/c/ming${OS_BUILD}/projects
#POSTGIS_TAG=trunk
# POSTGIS_MAJOR_VERSION=2
# POSTGIS_MINOR_VERSION=1
# POSTGIS_MICRO_VERSION=0SVN
version=${POSTGIS_MAJOR_VERSION}.${POSTGIS_MINOR_VERSION}.${POSTGIS_MICRO_VERSION}
# export PG_VER=9.2beta2
# export GEOS_VER=3.4.0dev
# export GDAL_VER=1.9.1
# export LIBXML_VER=2.7.8

if [ "$OS_BUILD" == "64" ] ; then
	export MINGHOST=x86_64-w64-mingw32
else
	export MINGHOST=i686-w64-mingw32
fi;

export PATHOLD=".:/bin:/include:/mingw/bin:/mingw/include:/c/ming${OS_BUILD}/imagemagick:/c/Windows/system32:/c/Windows:/usr/local/bin:/c/ming${OS_BUILD}/svn"
export WORKSPACE=`pwd`

echo PATH BEFORE: $PATH

export PGUSER=postgres

export PGPATH=${PROJECTS}/postgresql/rel/pg${PG_VER}w${OS_BUILD}


export GDAL_DATA="${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}/share/gdal"

export PROJ_VER=4.8.0
export PATH="${PATHOLD}:${PGPATH}/bin:${PGPATH}/lib"
export PATH="${MINGPROJECTS}/gettext/rel-gettext-0.18.1/bin:${MINGPROJECTS}/xsltproc:${MINGPROJECTS}/gtk/bin:${PROJECTS}/geos/rel-${GEOS_VER}w${OS_BUILD}/bin:${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}/bin:${MINGPROJECTS}/rel-libiconv-1.13.1w${OS_BUILD}/include:${MINGPROJECTS}/rel-libiconv-1.13.1w${OS_BUILD}/bin:${MINGPROJECTS}/proj/rel-${PROJ_VER}w${OS_BUILD}/bin:${MINGPROJECTS}/libxml/rel-libxml2-${LIBXML_VER}w${OS_BUILD}/bin:${PATH}"
echo PATH AFTER: $PATH

outdir="${PROJECTS}/postgis/src_dist/postgis-$version"
package="${PROJECTS}/postgis/src_dist/postgis-$version.tar.gz"

rm -rf $outdir
rm $package

if [ -d "$outdir" ]; then
	echo "Output directory $outdir already exists."
	exit 1
fi

echo "Exporting tag ${POSTGIS_TAG}"
svnurl="http://svn.osgeo.org/postgis/${POSTGIS_TAG}"
svn export $svnurl "$outdir"

if [ $? -gt 0 ]; then
	exit 1
fi

echo "Removing ignore files, make_dist.sh and HOWTO_RELEASE"
find "$outdir" -name .\*ignore -exec rm -v {} \;
rm -fv "$outdir"/make_dist.sh "$outdir"/HOWTO_RELEASE

# generating configure script and configuring
echo "Running autogen.sh; ./configure"
owd="$PWD"
cd "$outdir"
./autogen.sh
CPPFLAGS="-I${PGPATH}/pg${PG_VER}/include -I${MINGPROJECTS}/gettextrel-gettext-0.18.1/include -I${MINGPROJECTS}/rel-libiconv-1.13.1w${OS_BUILD}/include" \
LDFLAGS="-L${PGPATH}/lib -L${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}/lib -L${MINGPROJECTS}/gettext/rel-gettext-0.18.1/lib -L${MINGPROJECTS}/rel-libiconv-1.13.1w${OS_BUILD}/lib" ./configure \
 --host=${MINGHOST}  \
  --with-xml2config=${MINGPROJECTS}/libxml/rel-libxml2-${LIBXML_VER}w${OS_BUILD}/bin/xml2-config \
  --with-pgconfig=${PGPATH}/bin/pg_config \
  --with-geosconfig=${PROJECTS}/geos/rel-${GEOS_VER}w${OS_BUILD}/bin/geos-config \
  --with-projdir=${MINGPROJECTS}/proj/rel-${PROJ_VER}w${OS_BUILD} \
  --with-gdalconfig=${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}/bin/gdal-config \
  --with-libiconv=${MINGPROJECTS}/rel-libiconv-1.13.1w${OS_BUILD} \
  --with-xsldir=${MINGPROJECTS}/docbook/docbook-xsl-1.76.1 \
  --with-gui --with-gettext=no
# generating postgis_svn_revision.h for >= 2.0.0 tags 
if test -f utils/svn_repo_revision.pl; then 
	echo "Generating postgis_svn_revision.h"
	perl utils/svn_repo_revision.pl $svnurl
fi
#make
cd "$owd"

# generating comments
echo "Generating documentation"
owd="$PWD"
cd "$outdir"/doc
make comments
if [ $? -gt 0 ]; then
	exit 1
fi
make clean # won't drop the comment files
cd "$owd"

# Run make distclean
echo "Running make distclean"
owd="$PWD"
cd "$outdir"
make distclean
cd "$owd"
#svn export on windows outputs in native windows line breaks
#note suitable for unix users.
#This converts windows 2 unix linebreaks leaving binaries alone
cd "$outdir"
find . -name '*.*' | xargs dos2unix --safe;

echo "Generating $package file"
tar czf "$package" "$outdir"

#echo "Cleaning up"
#rm -Rf "$outdir"

