#!/bin/bash
set -e
#--passed in by Jenkins
#export OS_BUILD=64
#export GCC_TYPE=gcc48
#export PG_VER=9.3
#export LIBXML_VER=2.7.8
#export PGPORT=5443
export PGUSER=postgres


if [[ "${OS_BUILD}" == "" && "$1" == "" ]] ; then
    echo "Usage: makedependencies OS_BUILD"
    echo "       OS_BUILD = 32|64"
    echo "       or export OS_BUILD=xx"
    exit 1
fi

if [[ "${GCC_TYPE}" == "gcc48" ]] ; then
	export PROJECTS=/projects
	export MINGPROJECTS=/projects
	export PATHOLD="/mingw/bin:/mingw/include:/mingw/lib:/c/Windows/system32:/bin:/include:/usr/local/bin"
else
	export PROJECTS=/projects
	export MINGPROJECTS=/projects
	export PATHOLD="/mingw/bin:/mingw/include:/mingw/lib:/c/Windows/system32:/bin:/include:/usr/local/bin"
fi;
export PATHOLD="/mingw/bin:/mingw/include:/c/Windows/system32:/bin:/include"


cd ${PROJECTS}


export PGPATH=${PROJECTS}/postgresql/rel/pg${PG_VER}w${OS_BUILD}${GCC_TYPE}
export PGPATHEDB=${PROJECTS}/postgresql/rel/pg${PG_VER}w${OS_BUILD}${GCC_TYPE}edb

export SOURCE_FOLDER=${PROJECTS}/postgis/ogrfdw/branches/${OGRFDW_VER}
export GDAL_PATH=${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}${GCC_TYPE}
export GDAL_DATA="${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}${GCC_TYPE}/share/gdal"
export PATH="${PGPATH}/bin:${PGPATH}/lib:${PATHOLD}"
#make sure gtk and others are first in path -- 
# if you have a pkg-config installed in msys it will cause problems if found first before gtk one
PATH="${PATH}:${PROJECTS}/geos/rel-${GEOS_VER}w${OS_BUILD}${GCC_TYPE}/bin:${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}${GCC_TYPE}/bin:${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}${GCC_TYPE}/include:${PROJECTS}/rel-libiconv-1.13.1w${OS_BUILD}${GCC_TYPE}/lib:${PROJECTS}/rel-libiconv-1.13.1w${OS_BUILD}${GCC_TYPE}/include:${PROJECTS}/rel-libiconv-1.13.1w${OS_BUILD}${GCC_TYPE}/bin:${PROJECTS}/proj/rel-${PROJ_VER}w${OS_BUILD}${GCC_TYPE}/bin:${PROJECTS}/libxml/rel-libxml2-${LIBXML_VER}w${OS_BUILD}${GCC_TYPE}/bin:${PROJECTS}/gtkw${OS_BUILD}${GCC_TYPE}/bin"
echo PATH AFTER: $PATH


#-- build ogrfdw extension --
if true; then
	cd ${SOURCE_FOLDER}
	#https://docs.google.com/document/d/1qJjiNUleh220so1gwthTtyJuBZj0TD3eDMTql-djCk8/edit?pli=1
	make clean
	
	#remove the pgsql test because we don't build GDAL with PostgreSQL support
	sed -i 's/pgsql//' Makefile


	GDAL_CONFIG="${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}${GCC_TYPE}/bin/gdal-config"
	CFLAGS="-I${PGPATH}/include -I${PROJECTS}/rel-libiconv-1.13.1w${OS_BUILD}${GCC_TYPE}/include -L${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}${GCC_TYPE}/include"
	
	make SHLIB_LINK="-L${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}${GCC_TYPE}/lib -lpostgres -lpgport -lgdal" CPPFLAGS="-I.  -I${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}${GCC_TYPE}/include" 
	make install SHLIB_LINK="-L${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}${GCC_TYPE}/lib -lpostgres -lpgport -lgdal" CPPFLAGS="-I.  -I${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}${GCC_TYPE}/include"
	
	cp *.sql ${PGPATHEDB}/share/extension/
	cp *.control ${PGPATHEDB}/share/extension/
	cp *.dll ${PGPATHEDB}/lib
	cp *.exe ${PGPATHEDB}/bin
	
fi

if [ "$?" != "0" ]; then
	 exit $?
fi

#only package if make check succeeded
if [ "$?" == "0" ]; then
	cd ${SOURCE_FOLDER}
	strip *.dll
	export REL_PGVER=${PG_VER//./} #strip the period
	export RELDIR=${PROJECTS}/postgis/ogrfdw/builds
	export RELVERDIR=ogrfdw-pg${REL_PGVER}-binaries-${OGRFDW_VER}w${OS_BUILD}${GCC_TYPE}
	export PATH="${PATHOLD}:${PGPATH}/bin:${PGPATH}/lib"
	export outdir="${RELDIR}/${RELVERDIR}"
	export package="${RELDIR}/${RELVERDIR}.zip"
	export verfile="${RELDIR}/${RELVERDIR}/ogrfdw_version.txt"
	rm -rf $outdir
	rm -f $package
	mkdir -p $outdir
	mkdir -p $outdir/share/extension
	mkdir $outdir/lib
	mkdir $outdir/bin
	cp /c/ming${OS_BUILD}${GCC_TYPE}/mingw${OS_BUILD}/bin/libstdc++-6.dll $outdir/bin
	cp /c/ming${OS_BUILD}${GCC_TYPE}/mingw${OS_BUILD}/bin/libgcc*.dll $outdir/bin
	
	## copy GDAL dependencies 
	cp -p ${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}${GCC_TYPE}/bin/*.dll $outdir/bin
	cp -p ${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}${GCC_TYPE}/bin/ogr2ogr.exe $outdir/bin
	cp -p ${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}${GCC_TYPE}/bin/ogrinfo.exe $outdir/bin
	cp -rp  ${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}${GCC_TYPE}/share/gdal $outdir/gdal-data
  
  
	cp *.sql ${RELDIR}/${RELVERDIR}/share/extension
	cp -r *.control ${RELDIR}/${RELVERDIR}/share/extension
	cp -r *.dll ${RELDIR}/${RELVERDIR}/lib
	cp -r *.exe ${RELDIR}/${RELVERDIR}/bin
	cp -r LICENSE.md ${RELDIR}/${RELVERDIR}/ogrfdw_LICENSE.md
	cp -r README.md ${RELDIR}/${RELVERDIR}/ogrfdw_README.md
	
	
	echo "OGR_FDW: ${OGRFDW_VER}" > $verfile
	echo "PostgreSQL: ${PG_VER} w${OS_BUILD}" >> $verfile
	date_built="`eval date +%Y%m%d`"
	echo "Built: ${date_built}" >> $verfile
	echo "GIT_REPO: https://github.com/pramsey/pgsql-ogr-fdw" >> $verfile 
	echo "GIT_BRANCH: ${GIT_BRANCH}" >> $verfile
	echo "GIT_REVISION: ${GIT_COMMIT}" >> $verfile
	echo "GDAL_VER: ${GDAL_VER}" >> $verfile

	cd ${RELDIR}
	zip -r $package ${RELVERDIR}

	cp $package ${PROJECTS}/postgis/win_web/download/windows/pg${REL_PGVER}/buildbot/extras
fi

cd ${SOURCE_FOLDER}
#make installcheck
